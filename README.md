These commands are sample clients provided with the
XTrap X Server Extension, Version 3.3.

XTrap is an X Server extension which facilitates the capturing of
server protocol and synthesizing core input events.

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The master development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/app/xtrap

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches

